public class practice_3 {

    static int count;

    public static void main(String[] arr) {
        Student student1 = new Student();
        Student student2 = new Student("Vlasenko");
        Student student3 = new Student(21);
        Student student4 = new Student(21, "Vlasenko");

        student1.id = 21;
        student1.surname = "Vlasenko";

        student2.id = 21;

        student3.surname = "Vlasenko";

        student1.Print();
        student2.Print();
        student3.Print();
        student4.Print();
        System.out.printf("Count objects: %d\t", count);
    }

}

class Student {
    String surname;
    int id;

    Student() {
        countInc();
    }

    Student(String surname) {
        this.surname = surname;
        countInc();
    }

    Student(int id) {
        this.id = id;
        countInc();
    }

    Student(int id, String surname) {
        this.surname = surname;
        this.id = id;
        countInc();
    }

    public void Print() {
        System.out.printf("Student: %d\t", id);
        System.out.printf("Name: %s \tId: %d\n", surname, id);
    }

    public void countInc() {
        practice_3.count++;
    }

}

